import dawesometoolkit.*;
import unlekker.util.*;
import unlekker.modelbuilder.*;
import unlekker.modelbuilder.filter.*;
import unlekker.modelbuilder.UVertexList;
import processing.pdf.*;
import toxi.geom.*;


boolean record;


float alp = 1;
int counter = 0;
DawesomeToolkit dt;
ArrayList<Integer> colors;
ArrayList<PVector> nodeShape;
ArrayList<Node> nodes;
PImage resized;
PShape masterShape;

Tiler tiler;

float FOV=radians(90);
float CLIP_NEAR=1;
float CLIP_FAR=5000;
int NUM_TILES=15;

boolean isAdding = true;

float sWeight = 1.5;


UNav3D nav;

void setup() {
	size(792,1224,OPENGL);
	masterShape = loadShape("dovetail.svg");
	tiler=new Tiler((PGraphics3D)g,NUM_TILES);
	dt = new DawesomeToolkit(this);
	dt.enableLazySave();
	nav=new UNav3D(this);
	noiseDetail(3,0.5);
  nav.unregisterKeyEvents();
  nav.trans.set(0,0,-500);

	rectMode(CENTER);
	nodes = new ArrayList<Node>();
	resized = createImage(width,height,RGB);
}



void draw() {

	if (record) {
   	 beginRecord(PDF, "frame-####.pdf"); 
  	}

	background(255);

	smooth(8);

	tiler.pre();

	nav.doTransforms();
	for (Node n: nodes){
		n.display();
	}
	if (isAdding){
	float r = sin(counter/100.0)*2.8;
	r = noise(counter/50.0) * 5.0;
	float rot = radians(sin(counter/50.0)*r*40);
	color c = color(255);
	Node node = new Node(new PVector(0,0,(float)counter),c,r,rot);
	nodes.add(node);
	
	counter +=2;

	alp = 100+(sin(counter/100.0)*155);
	}
	
	
	
	tiler.post();
  if (record) {
    endRecord();
	record = false;
  }
	
}



void saveHiRes(int scaleFactor) {
  PGraphics hires = createGraphics(width*scaleFactor, height*scaleFactor, P3D);
  beginRecord(hires);
  hires.scale(scaleFactor);
  draw();
  endRecord();
  hires.save("hires.png");
}

void keyPressed() {
	if (key=='r'){
		sWeight *= (NUM_TILES/2);
  		tiler.initTiles(FOV,CLIP_NEAR,CLIP_FAR);
  		tiler.save(sketchPath("export"), "tiles-" + (System.currentTimeMillis() / 1000), "tga");
}

if (key=='a'){

	isAdding = !isAdding;
}

if (key=='p'){

	record = true;
}
  
}

class Tiler {
  protected PGraphics3D gfx;
  protected PImage buffer;
  protected Vec2D[] tileOffsets;
  protected double top;
  protected double bottom;
  protected double left;
  protected double right;
  protected Vec2D tileSize;

  protected int numTiles;
  protected int tileID;
  protected float subTileID;

  protected boolean isTiling;
  protected String fileName;
  protected String format;
  protected double cameraFOV;
  protected double cameraFar;
  protected double cameraNear;

  public Tiler(PGraphics3D g, int n) {
    gfx = g;
    numTiles = n;
  }

  public void chooseTile(int id) {
    Vec2D o = tileOffsets[id];
    gfx.frustum(o.x, o.x + tileSize.x, o.y, o.y + tileSize.y,
    (float) cameraNear, (float) cameraFar);
  }

  public void initTiles(float fov, float near, float far) {
    tileOffsets = new Vec2D[numTiles * numTiles];
    double aspect = (double) gfx.width / gfx.height;
    cameraFOV = fov;
    cameraNear = near;
    cameraFar = far;
    top = Math.tan(cameraFOV * 0.5) * cameraNear;
    bottom = -top;
    left = aspect * bottom;
    right = aspect * top;
    int idx = 0;
    tileSize = new Vec2D((float) (2 * right / numTiles), (float) (2 * top / numTiles));
    double y = top - tileSize.y;
    while (idx < tileOffsets.length) {
	double x = left;
	for (int xi = 0; xi < numTiles; xi++) {
	  tileOffsets[idx++] = new Vec2D((float) x, (float) y);
	  x += tileSize.x;
	}
	y -= tileSize.y;
    }
  }

  public void post() {
    if (isTiling) {
	subTileID += 0.5;
	if (subTileID > 1) {
	  int x = tileID % numTiles;
	  int y = tileID / numTiles;
	  gfx.loadPixels();
	  buffer.set(x * gfx.width, y * gfx.height, gfx);
	  if (tileID == tileOffsets.length - 1) {
	    buffer.save(fileName + "_" + buffer.width + "x"
		+ buffer.height + "." + format);
	    buffer = null;
	  }
	  subTileID = 0;
	  isTiling = (++tileID < tileOffsets.length);
	}
    }
  }

  public void pre() {
    if (isTiling) {
	chooseTile(tileID);
    }
  }

  public void save(String path, String baseName, String format) {
    (new File(path)).mkdirs();
    this.fileName = path + "/" + baseName;
    this.format = format;
    buffer = new PImage(gfx.width * numTiles, gfx.height * numTiles);
    tileID = 0;
    subTileID = 0;
    isTiling = true;
  }
  
  public boolean isTiling() {
    return isTiling;
  }
} 

