class Node {

	ArrayList<PVector> vectors;
	color nodeColor;
	PVector pos;
	PShape s;
	float scale;
	
	Node(PVector pos,color nodeColor, float scale, float rot){

		this.pos = pos;
		this.nodeColor = nodeColor;
		this.scale = scale;
		this.s = s;
	
		this.s = loadShape("dovetail.svg");
	
		s.scale(scale);
		s.rotate(rot);

	}

	void update(){

	}

	void display(){
		pushMatrix();
		translate(pos.x-(s.width/2), pos.y-(s.height/2), pos.z);
		noStroke();
		smooth(8);
		shape(s);
		popMatrix();

	}
}